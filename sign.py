import sys,datetime

class main():
   def __init__(self):
      print(" Date and Time of Session Start is %s" % (datetime.datetime.now()))
      self.arglength = len(sys.argv) - 1
      self.arguments = sys.argv
      main = self
      self.mainprocess(main)

   def mainprocess(self, main):
      print("Number of Arguments passed is %i" % (main.arglength), flush=True)
      main.ReadInputArguments = ReadInputArguments(main)
      if main.ReadInputArguments.stage == 'SIGN':
         main.sign = sign(main.ReadInputArguments)


class ReadInputArguments():
    def __init__(self, ms):
      self.stage = ms.arguments[1]
      self.path = ms.arguments[2]
      self.number = ms.arguments[3]

class sign():
   def __init__(self,pbuild):
      print("determining the sign of the number")
      self.sign_det(pbuild)

   def sign_det(self,pbuild):
      num = pbuild.number
      if num > 0:
         print("Positive number")
      elif num == 0:
         print("Zero")
      else:
         print("Negative number")

if __name__ == '__main__':
    main()

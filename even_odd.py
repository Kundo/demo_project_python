import sys, datetime

class main():
   def __init__(self):
      print(" Date and Time of Session Start is %s" % (datetime.datetime.now()))
      self.arglength = len(sys.argv) - 1
      self.arguments = sys.argv
      main = self
      self.mainprocess(main)

   def mainprocess(self, main):
      print("Number of Arguments passed is %i" % (main.arglength), flush=True)
      main.ReadInputArguments = ReadInputArguments(main)
      if main.ReadInputArguments.stage == 'EVENODD':
         main.even_odd = even_odd(main.ReadInputArguments)


class ReadInputArguments():
    def __init__(self, ms):
      self.stage = ms.arguments[1]
      self.path = ms.arguments[2]
      self.number = ms.arguments[3]

class even_odd():
   def __init__(self,pbuild):
      print("determining whether the number is even or odd")
      self.evenodd_det(pbuild)

   def evenodd_det(self,pbuild)
      num = pbuild.number
      num = int(input("Enter a number: "))
      if (num % 2) == 0:
         print("{0} is Even".format(num))
      else:
         print("{0} is Odd".format(num))

if __name__ == '__main__':
    main()
